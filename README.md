# CARTRACK #

This is a simple books catalogue API. The API supports the 4 basic CRUD actions and a search.

### What is this repository for? ###

* My implementation of Cartrack�s Backend candidate technical challenge
* Version 0.1.0
* [API docs powered by Swagger](http://167.172.191.199/api_doc.html)

### PHP Requirement
* PHP v7.4+ and later
* Postgres database v13

Make sure to create a database and have the credentials on hand for setup step below

### Dependencies

The API requires the following PHP extensions:

- [`pdo`](https://php.net/manual/en/book.soap.php)
- [`pgsql`](http://php.net/manual/en/book.openssl.php)
- [`json`](https://php.net/manual/en/book.json.php)

### How do I get set up? ###
1. Clone the repository to your local computer
```shell
git clone https://kenneth-onah@bitbucket.org/nanocraft/book-catalogue.git
```
2. Change into the repository directory
```shell
cd book-catalogue
```
3. Run composer to install all the dependencies
```shell
composer install
```
4. copy `.env.example` file to `.env` file and update database & JWT environment variables
```shell
cp .env.example .env
```

5. Run database migration script to create the table and populate table with demo data.
```shell
php database_migration.php
```

6. Start PHP dev server. API can can be accessed with clients such as [curl](https://curl.se/) or [Postman](https://www.postman.com/)
```shell
php -S localhost:8000 -t ./
```

## Tests

Install dependencies as mentioned above ([PHPUnit](http://packagist.org/packages/phpunit/phpunit) will also be installed).

Then execute below (from project root directory):

```bash
./vendor/bin/phpunit src/Tests/Controller/BookControllerTest.php
```

### Who do I talk to? ###

* You email repo admin (onah.kenneth@gmail.com)
* Connect on Skype (kenneth_onah)