<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

require './src/bootstrap.php';

use Cartrack\Etoll\Database\Connection;

$statement = <<<EOS
    SET statement_timeout = 0;
    SET lock_timeout = 0;
    SET client_encoding = 'UTF8';
    SET standard_conforming_strings = on;
    SET check_function_bodies = false;
    SET client_min_messages = warning;
    
    DROP TABLE IF EXISTS book;
    DROP TYPE IF EXISTS book_type;
    
    CREATE TYPE book_type AS ENUM ('paperback', 'hardback');
    
    CREATE TABLE IF NOT EXISTS book (
        id INT NOT NULL GENERATED ALWAYS AS IDENTITY,
        title CHARACTER VARYING NOT NULL,
        author CHARACTER VARYING NOT NULL,
        description TEXT DEFAULT NULL,
        type book_type,
        price NUMERIC(12,2),
        isbn_ean VARCHAR(13) NOT NULL,
        num_pages INT DEFAULT 0,
        genre CHARACTER VARYING(100) DEFAULT NULL,
        publisher CHARACTER VARYING,
        date_published INTEGER,
        PRIMARY KEY (id)
    );
    
    CREATE INDEX ON book ((lower(title)));

    INSERT INTO book
        (title, author, description, price, type, isbn_ean, num_pages, genre, publisher, date_published)
    VALUES
        ('The Atomic Power of God', 'Franklin Hall', 'Atomic Power with God, Thru Fasting and Prayer', 120, 'paperback', '9781614279464', 86, 'christian', 'Martino Publishing', 2016),
        ('Unraveling the Mystery of the blood covenant', 'John Osteen', NULL, 949, 'paperback', '9780912631349', 88, 'christian', 'John Osteen Publishing', 1987);
EOS;

try {
    $dbConnection = Connection::getNewInstance()->getConnection();
    $createTable = $dbConnection->exec($statement);

    echo "Database Migration Successfully!\n";
} catch (\PDOException $e) {
    exit($e->getMessage());
}