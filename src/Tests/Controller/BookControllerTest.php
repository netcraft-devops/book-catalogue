<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Tests\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

/**
 * Class BookControllerTest
 */
class BookControllerTest extends TestCase
{
    /**
     * @var Client
     */
    private static Client $client;

    /**
     * Setup before class is instantiated
     */
    public static function setUpBeforeClass(): void
    {
        // Create a mock and queue responses.
        $mock = new MockHandler([
            new Response(401, ['X-Foo' => 'Bar']),
            new Response(200, ['X-Foo' => 'Bar'], '{"success":true,"token":"vfmewrrmfjlhf"}'),
            new Response(200, ['X-Foo' => 'Bar'], '[
                    {
                        "id": 1,
                        "title": "The Atomic Power of God",
                        "author": "Franklin Hall",
                        "description": "Atomic Power with God, Thru Fasting and Prayer",
                        "type": "paperback",
                        "price": "120.00",
                        "isbn_ean": "9781614279464",
                        "num_pages": 86,
                        "genre": "christian",
                        "publisher": "Martino Publishing",
                        "date_published": 2016
                    }
                ]'),
            new Response(200, ['X-Foo' => 'Bar'], '{
                "id": 2,
                "title": "Unraveling the Mystery of the blood covenant",
                "author": "John Osteen",
                "description": null,
                "type": "paperback",
                "price": "949.00",
                "isbn_ean": "9780912631349",
                "num_pages": 88,
                "genre": "christian",
                "publisher": "John Osteen Publishing",
                "date_published": 1987
            }'),
            new Response(404, ['X-Foo' => 'Bar'], '{"error": "Resource not found"}'),
            new Response(200, ['X-Foo' => 'Bar'], '{"success": "1 book deleted successfully."}'),
            new Response(404, ['X-Foo' => 'Bar'], '{"error": "Resource not found"}'),
            new Response(200, ['X-Foo' => 'Bar'], '{"success": "Book successfully updated."}'),
            new Response(404, ['X-Foo' => 'Bar'], '{"error": "Resource not found"}'),
            new Response(200, ['X-Foo' => 'Bar'], '{"success": "Book created successfully"}'),
            new Response(422, ['X-Foo' => 'Bar'], '{"error": true, "message": "Fields required: title"}'),
            new Response(422, ['X-Foo' => 'Bar'], '{"error": true,"message": "Invalid property: titls"}'),
            new Response(200, ['X-Foo' => 'Bar'], '[
                {
                    "id": 1,
                    "title": "The Atomic Power of God",
                    "author": "Franklin Hall",
                    "description": "Atomic Power with God, Thru Fasting and Prayer",
                    "type": "paperback",
                    "price": "120.00",
                    "isbn_ean": "9781614279464",
                    "num_pages": 86,
                    "genre": "christian",
                    "publisher": "Martino Publishing",
                    "date_published": 2016
                }
            ]'),
            new Response(422, ['X-Foo' => 'Bar'], '{"error": true,"message": "Invalid property: titls"}')
        ]);

        $handlerStack = HandlerStack::create($mock);
        self::$client = new Client(['handler' => $handlerStack]);
    }
    /**
     * Test application entry point is blocked without token
     */
    public function testRequestWithoutJWTTokenFails()
    {
        $this->expectException(ClientException::class);
        $response = self::$client->request('GET', '/book');
        $this->assertEquals(401, $response->getStatusCode());
    }

    /**
     * Test get auth token
     */
    public function testGetJWTTokenSucceeds()
    {
        $response = self::$client->request('GET', '/book/getToken');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('token', $response->getBody());
    }

    /**
     * Test get all books
     */
    public function testGetAllBooks()
    {
        $response = self::$client->request('GET', '/book');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson('[
                    {
                        "id": 1,
                        "title": "The Atomic Power of God",
                        "author": "Franklin Hall",
                        "description": "Atomic Power with God, Thru Fasting and Prayer",
                        "type": "paperback",
                        "price": "120.00",
                        "isbn_ean": "9781614279464",
                        "num_pages": 86,
                        "genre": "christian",
                        "publisher": "Martino Publishing",
                        "date_published": 2016
                    }
                ]', $response->getBody());
    }

    /**
     * Test get a book with an id
     */
    public function testGetABook()
    {
        $response = self::$client->request('GET', '/book/2');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('"id": 2', $response->getBody());
    }

    /**
     * Test get a book not found
     */
    public function testGetBookNotFound()
    {
        $this->expectException(ClientException::class);
        $response = self::$client->request('GET', '/book/99');
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Test deleting an existing book succeeds
     */
    public function testDeleteBookSuccess()
    {
        $response = self::$client->request('DELETE', '/book/1');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('book deleted successfully', $response->getBody());
    }

    /**
     * Test deleting an non-existent book fails
     */
    public function testDeleteBookNotFoundFails()
    {
        $this->expectException(ClientException::class);
        $response = self::$client->request('DELETE', '/book/99');
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Test updating an existing book succeeds
     */
    public function testUpdateBookSuccess()
    {
        $response = self::$client->request('PUT', '/book/1');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('Book successfully updated.', $response->getBody());
    }

    /**
     * Test updating an non-existent book fails
     */
    public function testUpdateBookNotFoundFails()
    {
        $this->expectException(ClientException::class);
        $response = self::$client->request('PUT', '/book/99');
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Test creating a book succeeds
     */
    public function testCreateBookSuccess()
    {
        $response = self::$client->request('POST', '/book');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('Book created successfully', $response->getBody());
    }

    /**
     * Test attribute validation when creating a book
     */
    public function testCreateBookWithInvalidAttributesFails()
    {
        $this->expectException(ClientException::class);
        $response = self::$client->request('POST', '/book');
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertStringContainsString('Fields required', $response->getBody());
    }

    /**
     * Test updating an book with invalid attribute fails
     */
    public function testUpdateBookWithInvalidAttributeFails()
    {
        $this->expectException(ClientException::class);
        $response = self::$client->request('PUT', '/book/1');
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertStringContainsString('Invalid property', $response->getBody());
    }

    /**
     * Test searching for a book found
     */
    public function testSearchFoundABook()
    {
        $response = self::$client->request('PUT', '/book/search?title=The Atomic Power of God');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson('{
                    "id": 1,
                    "title": "The Atomic Power of God",
                    "author": "Franklin Hall",
                    "description": "Atomic Power with God, Thru Fasting and Prayer",
                    "type": "paperback",
                    "price": "120.00",
                    "isbn_ean": "9781614279464",
                    "num_pages": 86,
                    "genre": "christian",
                    "publisher": "Martino Publishing",
                    "date_published": 2016
                }', $response->getBody());
    }

    /**
     * Test searching for a book not found
     */
    public function testSearchBookNotFound()
    {
        $this->expectException(ClientException::class);
        $response = self::$client->request('PUT', '/book/search?title=anthony');
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertStringContainsString('Resource not found', $response->getBody());
    }
}