<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Database;

use Cartrack\Etoll\Contracts\Database\ConnectionInterface;
use PDO;

/**
 * Class Connector
 * @package Cartrack\Etoll\Database
 */
class Connection implements ConnectionInterface
{

    /**
     * @var ?PDO
     */
    private static ?PDO $dbConnection = null;

    /**
     * Connector constructor.
     */
    private function __construct()
    {
        $host = $_ENV['DB_HOST'] ?? 'localhost';
        $port = $_ENV['DB_PORT'] ?? '5432';
        $database = $_ENV['DB_DATABASE'];
        $username = $_ENV['DB_USERNAME'];
        $password = $_ENV['DB_PASSWORD'];

        $dsn = "pgsql:host=$host;port=$port;dbname=$database";

        try {
            if (!self::$dbConnection) {
                self::$dbConnection = new PDO(
                    $dsn,
                    $username,
                    $password,
                    [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
                );
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    /**
     * @return self
     */
    public static function getNewInstance(): self
    {
        return new self();
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        $self = self::getNewInstance();

        return $self::$dbConnection;
    }
}
