<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Database;

class ConnectionFactory
{
    /**
     * @var ?string
     */
    protected ?string $instanceName;

    /**
     * BookFactory constructor.
     * @param string $instanceName
     */
    public function __construct(string $instanceName = Connection::class)
    {
        $this->instanceName = $instanceName;
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return $this->instanceName::getNewInstance();
    }
}