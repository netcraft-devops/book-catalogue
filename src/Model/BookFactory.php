<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Model;

class BookFactory
{
    /**
     * @var ?string
     */
    protected ?string $instanceName;

    /**
     * BookFactory constructor.
     * @param string $instanceName
     */
    public function __construct(string $instanceName = Book::class)
    {
        $this->instanceName = $instanceName;
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return new $this->instanceName;
    }
}