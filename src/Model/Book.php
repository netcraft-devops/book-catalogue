<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Model;

use Cartrack\Etoll\Database\Connection;
use Cartrack\Etoll\Model\TableGateways\BookGateway;
use Cartrack\Etoll\Model\TableGateways\BookGatewayFactory;

class Book
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /**
     * @var string
     */
    protected string $scenario = self::SCENARIO_CREATE;

    /**
     * @var string[]
     */
    protected array $attributes = [
        'title', 'author', 'description',
        'type', 'price', 'isbn_ean', 'num_pages',
        'genre', 'publisher', 'date_published'
    ];

    /**
     * @var string[]
     */
    protected array $fields = [
        'title', 'author',
        'type', 'price', 'isbn_ean',
        'publisher', 'date_published'
    ];

    /**
     * @var ?BookGateway
     */
    protected ?BookGateway $bookGateway;

    /**
     * Book constructor.
     */
    public function __construct(?BookGateway $bookGateway = null)
    {
        $this->bookGateway = $bookGateway ?? (new BookGatewayFactory())->create();
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->bookGateway->findAll();
    }

    /**
     * @param string $id
     * @return array
     */
    public function get(string $id): array
    {
        return $this->bookGateway->find($id);
    }

    /**
     * @param array $data
     * @return int
     */
    public function create(array $data): int
    {
        $columnKey = array_keys($data);
        $valuesData = array_values($data);

        return $this->bookGateway->insert($columnKey, $valuesData);
    }

    /**
     * @param string $id
     * @param array $data
     * @return int
     */
    public function update(string $id, array $data): int
    {
        return $this->bookGateway->update($id, $data);
    }

    /**
     * @param string $id
     * @return int
     */
    public function delete(string $id): int
    {
        return $this->bookGateway->delete($id);
    }

    /**
     * @param string $id
     * @return array
     */
    public function search(string $id): array
    {
        return $this->bookGateway->search($id);
    }

    /**
     * @param array $input
     * @return array|bool
     */
    public function validate(array $input)
    {
        $valid = [];
        $keys = array_keys($input);

        if ($this->scenario === self::SCENARIO_CREATE) {
            $valid = array_diff($this->fields, $keys);

            if (!empty($valid)) {
                return $valid;
            }
        }

        if ($this->scenario === self::SCENARIO_UPDATE) {
            $valid = array_intersect($this->attributes, $keys);

            if (count($keys) !== count($valid)) {
                return array_diff($keys, $valid);
            }
        }

        return true;
    }

    /**
     * @param string $scenario
     * @return $this
     */
    public function setScenario(string $scenario): Book
    {
        $this->scenario = $scenario;

        return $this;
    }
}