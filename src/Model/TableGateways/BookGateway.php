<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Model\TableGateways;

use Cartrack\Etoll\Contracts\Database\ConnectionInterface;
use Cartrack\Etoll\Contracts\Model\DAOInterface;
use Cartrack\Etoll\Database\ConnectionFactory;
use PDO;
use PDOException;

class BookGateway implements DAOInterface
{
    /**
     * @var ?PDO
     */
    private ?PDO $db;

    /**
     * BookGateway constructor.
     * @param ?ConnectionInterface $db
     */
    public function __construct(?ConnectionInterface $db = null)
    {
        $this->db = $db ? $db->getConnection() : (new ConnectionFactory)->create()->getConnection();
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $query = "
            SELECT *
            FROM book;
        ";

        try {
            $statement = $this->db->query($query);

            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * @param string $id
     * @return array
     */
    public function find(string $id): array
    {
        $query = "
            SELECT *
            FROM book
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($query);
            $statement->bindParam(':id', $id, PDO::PARAM_INT);
            $statement->execute();

            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * @param array $columns
     * @param array $values
     * @return int
     */
    public function insert(array $columns, array $values): int
    {
        $column = implode(', ', $columns);
        $params = array_map(function ($data) {
            return ':' . $data;
            }, $columns);
        $params = implode(', ', $params);

        $statement = "
            INSERT INTO book ({$column})
            VALUES ({$params});
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array_combine($columns, $values));

            return $statement->rowCount();
        } catch (PDOException $e) {
            return -1;
        }
    }

    /**
     * @param int $id
     * @param array $params
     * @return int
     */
    public function update(int $id, array $params): int
    {
        $set = '';

        foreach ($params as $key => $value) {
            $set .= $key . ' = ' . ' :' . $key . ', ';
        }

        $set = trim($set, ', ');

        $statement = "
            UPDATE book
            SET {$set}
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array_merge(
                ['id' => $id], $params
            ));

            return $statement->rowCount();
        } catch (PDOException $e) {
            return -1;
        }
    }

    /**
     * @param string $id
     * @return int
     */
    public function delete(string $id): int
    {
        $statement = "
            DELETE FROM book
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->bindParam(':id', $id, PDO::PARAM_INT);
            $statement->execute();

            return $statement->rowCount();
        } catch (PDOException $e) {
            return -1;
        }
    }

    /**
     * @param string $param
     * @return array
     */
    public function search(string $param): array
    {
        $query = "
            SELECT *
            FROM book
            WHERE title LIKE :title;
        ";
        $param = "%{$param}%";

        try {
            $statement = $this->db->prepare($query);
            $statement->bindParam(':title', $param, PDO::PARAM_STR);
            $statement->execute();

            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }
}