<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Controller\Book;

use Cartrack\Etoll\Model\Book;
use Cartrack\Etoll\Model\BookFactory;

/**
 * Class IndexController
 */
class IndexController
{
    const HTTP_STATUS_FAIL = -1;
    const HTTP_STATUS_OK = 'HTTP/1.1 201 Created';
    const HTTP_STATUS_NOT_FOUND = 'HTTP/1.1 404 Not Found';
    const HTTP_STATUS_UNPROCESSABLE_ENTITY = 'HTTP/1.1 422 Unprocessable Entity';

    /**
     * @var ?string
     */
    private ?string $bookId;

    /**
     * @var Book
     */
    private Book $book;

    /**
     * @var string
     */
    private string $requestMethod;

    /**
     * IndexController constructor.
     * @param string $requestMethod
     * @param string|null $bookId
     */
    public function __construct(string $requestMethod, ?string $bookId, Book $book = null)
    {
        $this->bookId = $bookId;
        $this->requestMethod = $requestMethod;
        $this->book = $book ?? (new BookFactory)->create();
    }

    /**
     * @return string
     */
    public function processRequest(): string
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->bookId && is_numeric($this->bookId)) {
                    $response = $this->getBook($this->bookId);
                } elseif ($this->bookId && is_string($this->bookId) && $this->bookId === 'search') {
                    $title = $_GET['title'] ?? '';
                    $response = $this->search($title);
                } else {
                    $response = $this->getAllBooks();
                }

                break;
            case 'POST':
                $response = $this->createBookFromRequest();

                break;
            case 'PUT':
                $response = $this->updateBookFromRequest($this->bookId);

                break;
            case 'DELETE':
                $response = $this->deleteBook($this->bookId);

                break;
            default:
                $response = $this->notFoundResponse();

                break;
        }

        header($response['status_code_header']);

        return $response['body'];
    }

    /**
     * @return array
     */
    private function createBookFromRequest(): array
    {
        $response = [
            'status_code_header' => self::HTTP_STATUS_UNPROCESSABLE_ENTITY,
            'body' => json_encode([
                'success' => true,
                'message' => 'Book successfully'
            ])
        ];

        $input = json_decode(file_get_contents('php://input'), true);
        $result = $this->book->validate($input);

        if (is_array($result)) {
            $message = 'Fields required: ' . implode(', ', $result);
            return $this->unprocessableEntityResponse($message);
        }

        $result = $this->book->create($input);

        if ($result !== self::HTTP_STATUS_FAIL) {
            $response['status_code_header'] = self::HTTP_STATUS_OK;
            $response['body'] = json_encode([
                'success' => true,
                'message' => 'Book created successfully'
            ]);
        }

        return $response;
    }

    /**
     * @param ?string $message
     * @return array
     */
    private function unprocessableEntityResponse(?string $message = ''): array
    {
        $response['status_code_header'] = self::HTTP_STATUS_UNPROCESSABLE_ENTITY;
        $response['body'] = json_encode([
            'error' => true,
            'message' => $message ?? 'Invalid input'
        ]);

        return $response;
    }

    /**
     * @param string $id
     * @return array
     */
    private function updateBookFromRequest(string $id): array
    {
        $result = $this->book->get($id);

        if (!$result) {
            return $this->notFoundResponse();
        }

        $response = [
            'status_code_header' => self::HTTP_STATUS_UNPROCESSABLE_ENTITY,
            'body' => json_encode([
                'success' => true,
                'message' => 'Book update failed'
            ])
        ];

        $input = (array)json_decode(file_get_contents('php://input'), TRUE);
        $this->book->setScenario('update');
        $result = $this->book->validate($input);

        if (is_array($result)) {
            $message = 'Invalid property: ' . implode(', ', $result);
            return $this->unprocessableEntityResponse($message);
        }

        $result = $this->book->update($id, $input);

        if ($result !== self::HTTP_STATUS_FAIL) {
            $response['status_code_header'] = self::HTTP_STATUS_OK;
            $response['body'] = json_encode([
                'success' => true,
                'message' => 'Book successfully updated'
            ]);
        }

        return $response;
    }

    /**
     * @return array
     */
    private function notFoundResponse(): array
    {
        $response['status_code_header'] = self::HTTP_STATUS_NOT_FOUND;
        $response['body'] = json_encode([
            'error' => 'Resource not found'
        ]);

        return $response;
    }

    /**
     * @param string $id
     * @return array
     */
    private function deleteBook(string $id): array
    {
        $result = $this->book->get($id);

        if (!$result) {
            return $this->notFoundResponse();
        }

        $count = $this->book->delete($id);
        $response['status_code_header'] = self::HTTP_STATUS_OK;
        $response['body'] = json_encode([
            'success' => "$count book deleted successfully."
        ]);

        return $response;
    }

    /**
     * @return array
     */
    private function getAllBooks(): array
    {
        $result = $this->book->all();
        $response['status_code_header'] = self::HTTP_STATUS_OK;
        $response['body'] = json_encode($result);

        return $response;
    }

    /**
     * @param string $id
     * @return array
     */
    private function getBook(string $id): array
    {
        $result = $this->book->get($id);

        if (!$result) {
            return $this->notFoundResponse();
        }

        $response['status_code_header'] = self::HTTP_STATUS_OK;
        $response['body'] = json_encode($result);

        return $response;
    }

    /**
     * @param string $search
     * @return array
     */
    private function search(string $search): array
    {
        $result = $this->book->search($search);

        if (!$result) {
            return $this->notFoundResponse();
        }

        $response['status_code_header'] = self::HTTP_STATUS_OK;
        $response['body'] = json_encode($result);

        return $response;
    }
}