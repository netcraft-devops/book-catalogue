<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Controller;

use Cartrack\Etoll\Controller\Book\IndexController;
use ReallySimpleJWT\Token;

/**
 * Class FrontController
 * @package Cartrack\Etoll\Controller
 */
class FrontController
{
    /**
     * @return string
     */
    public function dispatch(): string
    {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri = explode('/', $uri);

        // All of our endpoints start with /cartrack
        // everything else results in a 404 Not Found
        if ($uri[1] !== 'book') {
            header("HTTP/1.1 404 Not Found");
            exit();
        }

        // Return token to client for subsequent requests
        if ((string)$uri[2] === 'getToken') {
            header("HTTP/1.1 200 OK");
            echo json_encode([
                'success' => true,
                'token' => $this->createToken()
            ]);
            exit();
        }

        // Authenticate client token:
        if (!$this->validateToken()) {
            header("HTTP/1.1 401 Unauthorized");
            echo json_encode([
                'error' => true,
                'message' => 'Unauthorized'
            ]);
            exit();
        }

        // The user id is, of course, optional and must be a number:
        $bookId = null;

        if (isset($uri[2])) {
            $bookId = $uri[2];
        }

        $requestMethod = $_SERVER["REQUEST_METHOD"];

        // Pass the request method and book ID to the BookController and process the HTTP request:
        $controller = new IndexController($requestMethod, $bookId);

        return $controller->processRequest();
    }

    /**
     * @return string
     */
    protected function createToken(): string
    {
        $payload = [
            'iat' => time(),
            'uid' => 1,
            'exp' => time() + 10,
            'iss' => $_ENV['JWT_ISSUER'] ?? 'localhost'
        ];
        $secret = $_ENV['JWT_SECRET'];

        return Token::customPayload($payload, $secret);
    }

    /**
     * @return bool
     */
    protected function validateToken(): bool
    {
        try {
            switch (true) {
                case array_key_exists('HTTP_AUTHORIZATION', $_SERVER) :
                    $authHeader = $_SERVER['HTTP_AUTHORIZATION'];
                    break;
                case array_key_exists('Authorization', $_SERVER) :
                    $authHeader = $_SERVER['Authorization'];
                    break;
                default :
                    $authHeader = null;
                    break;
            }

            preg_match('/Bearer\s(\S+)/', $authHeader, $matches);

            if (!isset($matches[1])) {
                throw new \Exception('No Bearer Token');
            }

            $secret = $_ENV['JWT_SECRET'];

            return Token::validate($matches[1], $secret);
        } catch (\Exception $e) {
            return false;
        }
    }
}