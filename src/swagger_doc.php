<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *     title="Books Catalogue API",
 *     version="1.0.0",
 *     description="Cartrack PHP Tech challenge"
 * )
 */
/**
 * @OA\SecurityScheme(
 *     securityScheme="Token",
 *     type="http",
 *     description="A token for making HTTP requests",
 *     scheme="bearer",
 *     bearerFormat="JWT",
 *     in="header"
 * )
 */
/**
 * @OA\Get(
 *     path="/book/getToken",
 *     summary="Returns JWT token",
 *     @OA\Response(
 *         response=200,
 *         description="Returns JWT token to be used in subsequent requests!"
 *     )
 * )
 */
/**
 * @OA\Get(
 *     path="/book",
 *     summary="Get all books",
 *     @OA\Response(
 *         response=200,
 *         description="Successful operation"
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized access"
 *     )
 * )
 */
/**
 * @OA\Get(
 *     path="/book/:bookId",
 *     description="Get a book",
 *     @OA\Parameter(
 *         name="bookId",
 *         in="query",
 *         required=true,
 *         @OA\Schema(type="string")
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Details of the book whose id is provided"
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Book not found"
 *     )
 * )
 */
/**
 * @OA\Delete(
 *     path="/book/:bookId",
 *     description="Delete a book",
 *     @OA\Parameter(
 *         name="bookId",
 *         in="query",
 *         required=true,
 *         @OA\Schema(type="string")
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Deleted book whose id is provided from the catalogue"
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Book not found"
 *     )
 * )
 */
/**
 * @OA\Post(
 *     path="/book",
 *     description="Create a book",
 *     @OA\Response(
 *         response=200,
 *         description="Added a new book to the catalogue"
 *     )
 * )
 */
/**
 * @OA\Put(
 *     path="/book",
 *     description="Update a book",
 *     @OA\Response(
 *         response="default",
 *         description="Change any details of a book"
 *     )
 * )
 */
/**
 * @OA\Get(
 *     path="/book/search",
 *     summary="Returns most accurate search result object",
 *     description="Search for a book, if found return it!",
 *     @OA\Parameter(
 *         name="title",
 *         in="query",
 *         required=true,
 *         @OA\Schema(type="string")
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Book details"
 *     )
 * )
 */