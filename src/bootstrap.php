<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 *
 * @category Cartrack
 * @package e-toll-system
 * @copyright Copyright &copy; 2021 Cartrack
 * @author Kenneth Onah <onah.kenneth@gmail.com>
 */

require 'vendor/autoload.php';

use Dotenv\Dotenv;

$dotenv = DotEnv::createMutable(dirname(__DIR__));
$dotenv->safeLoad();