<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Contracts\Model;


interface DAOInterface
{
    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @param string $id
     * @return array
     */
    public function find(string $id): array;

    /**
     * @param array $columns
     * @param array $values
     * @return int
     */
    public function insert(array $columns, array $values): int;

    /**
     * @param int $id
     * @param array $params
     * @return int
     */
    public function update(int $id, array $params): int;

    /**
     * @param string $id
     * @return int
     */
    public function delete(string $id): int;

    /**
     * @param string $param
     * @return array
     */
    public function search(string $param): array;
}