<?php
/**
 * Copyright &copy; 2021 Cartrack. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Cartrack\Etoll\Contracts\Database;

use PDO;

/**
 * Interface ConnectorInterface
 * @package Cartrack\Etoll\Contracts\Database
 */
interface ConnectionInterface
{
    /**
     * @return PDO
     */
    public function getConnection(): PDO;
}